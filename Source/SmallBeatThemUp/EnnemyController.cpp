// Fill out your copyright notice in the Description page of Project Settings.


#include "EnnemyController.h"
#include "GameFramework/Character.h"


AEnnemyController::AEnnemyController() {
	PrimaryActorTick.bCanEverTick = true;
}

void AEnnemyController::BeginPlay() {
	Super::BeginPlay();
}

void AEnnemyController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void AEnnemyController::Move() {
	ACharacter* player = Cast<ACharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (player == nullptr) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Didn't find the player in EnnemyController !"));
		return;
	}
	APawn* pawn = GetPawn();
	if (pawn == nullptr) {
        //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Didn't find the pawn in EnnemyController !"));
        return;
    }
	FVector playerPos = player->GetActorLocation();
	FVector currentPos = pawn->GetActorLocation();
	FVector direction = playerPos - currentPos;
	direction.Normalize();
	pawn->AddMovementInput(direction, 1.f);
}
