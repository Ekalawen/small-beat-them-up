// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnnemyController.generated.h"

/**
 * 
 */
UCLASS()
class SMALLBEATTHEMUP_API AEnnemyController : public AAIController
{
	GENERATED_BODY()

public:
	AEnnemyController();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	void Move();
};
