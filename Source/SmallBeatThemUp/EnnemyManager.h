
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "EnnemyManager.generated.h"

UCLASS()
class SMALLBEATTHEMUP_API AEnnemyManager : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ennemy", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<ACharacter> ennemyBP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ennemy", meta = (AllowPrivateAccess = "true"))
    int ennemyCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ennemy", meta = (AllowPrivateAccess = "true"))
    float spawnRate = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ennemy", meta = (AllowPrivateAccess = "true"))
    FVector spawnBounds;

	TArray<ACharacter*> ennemies;
	
public:	
	AEnnemyManager();

	virtual void BeginPlay() override;

	void SpawnEnnemies();
	void SpawnOneEnnemy();
	FVector GetRandomLocation();

	virtual void Tick(float DeltaTime) override;
};
