// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"

// Sets default values
ABullet::ABullet()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	sphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	sphereComponent->SetSphereRadius(collisionRadius);
	sphereComponent->SetCollisionProfileName("Trigger");
	RootComponent = sphereComponent;
	sphereComponent->SetGenerateOverlapEvents(true);


	sphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ABullet::OnOverlapBegin);
	//sphereComponent->OnComponentHit.AddDynamic(this, &ABullet::OnHit);
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	
	SetLifeSpan(1.0f);
}

// Called every frame
void ABullet::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	FVector currentPosition = GetActorLocation();

	FVector newPosition = currentPosition + direction * speed * DeltaTime;

	SetActorLocation(newPosition);
}

void ABullet::SetDirection(FVector newDirection) {
	direction.Set(newDirection.X, newDirection.Y, 0.);
}


void ABullet::SetShooter(ACharacter* newShooter) {
    shooter = newShooter;
}

// Called to bind functionality to input
void ABullet::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABullet::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	// Don't destroy the owner
	if (OtherActor->Tags.Contains("Player")) {
        return;
    }
	// Destroy only ennemies
	if (!OtherActor->Tags.Contains("Ennemy")) {
		return;
    }
    GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Hit !"));
	OtherActor->Destroy();
	Destroy();
}


