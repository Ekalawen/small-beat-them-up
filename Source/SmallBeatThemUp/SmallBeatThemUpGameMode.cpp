// Copyright Epic Games, Inc. All Rights Reserved.

#include "SmallBeatThemUpGameMode.h"
#include "SmallBeatThemUpPlayerController.h"
#include "SmallBeatThemUpCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASmallBeatThemUpGameMode::ASmallBeatThemUpGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASmallBeatThemUpPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}

void ASmallBeatThemUpGameMode::BeginPlay() {
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Spawning EnnemyManager ! :)"));
	ennemyManager = GetWorld()->SpawnActor<AEnnemyManager>(ennemyManagerBP, FVector(0, 0, 0), FRotator(0, 0, 0));
}

