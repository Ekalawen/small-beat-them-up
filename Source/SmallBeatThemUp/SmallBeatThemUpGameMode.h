// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EnnemyManager.h"
#include "SmallBeatThemUpGameMode.generated.h"

UCLASS(minimalapi)
class ASmallBeatThemUpGameMode : public AGameModeBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Managers", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<AEnnemyManager> ennemyManagerBP;

	AEnnemyManager* ennemyManager;

public:
	ASmallBeatThemUpGameMode();

	virtual void BeginPlay() override;
};



