// Copyright Epic Games, Inc. All Rights Reserved.

#include "SmallBeatThemUp.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SmallBeatThemUp, "SmallBeatThemUp" );

DEFINE_LOG_CATEGORY(LogSmallBeatThemUp)
 