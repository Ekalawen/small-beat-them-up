

#include "EnnemyManager.h"

AEnnemyManager::AEnnemyManager()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AEnnemyManager::BeginPlay()
{
	Super::BeginPlay();

	SpawnEnnemies();

	FTimerHandle MemberTimerHandle;
	GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &AEnnemyManager::SpawnOneEnnemy, spawnRate, true, 0.0f);
}

void AEnnemyManager::SpawnEnnemies() {
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Spawning Ennemies ! :)"));
	for (int i = 0; i < ennemyCount; ++i) {
		SpawnOneEnnemy();
	}
}

void AEnnemyManager::SpawnOneEnnemy() {
	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;
	FVector spawnLocation = GetRandomLocation();
	FRotator spawnRotation = FRotator(0.f, 0.f, 0.f);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("SpawningEnnemy"));
	ACharacter* ennemy = GetWorld()->SpawnActor<ACharacter>(ennemyBP, spawnLocation, spawnRotation, spawnParams);
	ennemies.Add(ennemy);
}

FVector AEnnemyManager::GetRandomLocation() {
	float x = FMath::RandRange(0., spawnBounds.X);
    float y = FMath::RandRange(0., spawnBounds.Y);
	float z = spawnBounds.Z;
    return FVector(x, y, z);
}

void AEnnemyManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

