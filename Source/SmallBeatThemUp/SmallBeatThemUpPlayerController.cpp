// Copyright Epic Games, Inc. All Rights Reserved.

#include "SmallBeatThemUpPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "SmallBeatThemUpCharacter.h"
#include "Engine/World.h"

ASmallBeatThemUpPlayerController::ASmallBeatThemUpPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
}

void ASmallBeatThemUpPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	//MoveWithMouse(DeltaTime);
	MoveWithKeyboard(DeltaTime);
}

void ASmallBeatThemUpPlayerController::MoveWithKeyboard(float DeltaTime) {
	APawn* const pawn = GetPawn();
	if (!pawn) {
		return;
	}
	FVector direction = FVector(InputComponent->GetAxisValue("Vertical"), InputComponent->GetAxisValue("Horizontal"), 0.f);
	direction.Normalize();
	pawn->AddMovementInput(direction, 1.f, false);
}

void ASmallBeatThemUpPlayerController::MoveWithMouse(float DeltaTime)
{
	if (bInputPressed)
	{
		FollowTime += DeltaTime;

		// Look for the touch location
		FVector HitLocation = FVector::ZeroVector;
		FHitResult Hit;
		if (bIsTouch)
		{
			GetHitResultUnderFinger(ETouchIndex::Touch1, ECC_Visibility, true, Hit);
		}
		else
		{
			GetHitResultUnderCursor(ECC_Visibility, true, Hit);
		}
		HitLocation = Hit.Location;

		// Direct the Pawn towards that location
		APawn* const MyPawn = GetPawn();
		if (MyPawn)
		{
			FVector WorldDirection = (HitLocation - MyPawn->GetActorLocation()).GetSafeNormal();
			MyPawn->AddMovementInput(WorldDirection, 1.f, false);
		}
	}
	else
	{
		FollowTime = 0.f;
	}
}

void ASmallBeatThemUpPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	//InputComponent->BindAction("SetDestination", IE_Pressed, this, &ASmallBeatThemUpPlayerController::OnSetDestinationPressed);
	//InputComponent->BindAction("SetDestination", IE_Released, this, &ASmallBeatThemUpPlayerController::OnSetDestinationReleased);

	////support touch devices 
	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ASmallBeatThemUpPlayerController::OnTouchPressed);
	//InputComponent->BindTouch(EInputEvent::IE_Released, this, &ASmallBeatThemUpPlayerController::OnTouchReleased);

	// My axis
	InputComponent->BindAxis("Horizontal");
	InputComponent->BindAxis("Vertical");
	InputComponent->BindAction("Shoot", IE_Pressed, this, &ASmallBeatThemUpPlayerController::Shoot);
}

void ASmallBeatThemUpPlayerController::Shoot() {
	FVector hitLocation = FVector::ZeroVector;
	FHitResult Hit;
    GetHitResultUnderCursor(ECC_Visibility, true, Hit);
	hitLocation = Hit.Location;
	APawn* const pawn = GetPawn();
	if (!pawn) {
		return;
	}
    FVector worldDirection = (hitLocation - pawn->GetActorLocation()).GetSafeNormal();
	ASmallBeatThemUpCharacter* character = Cast<ASmallBeatThemUpCharacter>(pawn);
	if (!character) {
        return;
    }
	character->Shoot(worldDirection);
}

void ASmallBeatThemUpPlayerController::OnSetDestinationPressed()
{
	// We flag that the input is being pressed
	bInputPressed = true;
	// Just in case the character was moving because of a previous short press we stop it
	StopMovement();
}

void ASmallBeatThemUpPlayerController::OnSetDestinationReleased()
{
	// Player is no longer pressing the input
	bInputPressed = false;

	// If it was a short press
	if(FollowTime <= ShortPressThreshold)
	{
		// We look for the location in the world where the player has pressed the input
		FVector HitLocation = FVector::ZeroVector;
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, true, Hit);
		HitLocation = Hit.Location;

		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, HitLocation);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, HitLocation, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}
}

void ASmallBeatThemUpPlayerController::OnTouchPressed(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	bIsTouch = true;
	OnSetDestinationPressed();
}

void ASmallBeatThemUpPlayerController::OnTouchReleased(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	bIsTouch = false;
	OnSetDestinationReleased();
}
